use futures::StreamExt;
use std::io::Write;
use std::path::{Path, PathBuf};
use std::time::{SystemTime, UNIX_EPOCH};

use actix_multipart::Multipart;
use actix_web::web;
use serde::Serialize;

use crate::api::groups::GroupQuery;
use crate::api::JsonError;
use crate::db;
use crate::env::Env;
use crate::models::{Group, Homework, HomeworkResource, HomeworkResourceInput};

#[derive(Serialize)]
pub struct HomeworkResourcesResponse {
    resources: Vec<HomeworkResource>,
}

pub async fn get_homework_resources(
    db: web::Data<db::PgPool>,
    homework_id: web::Path<i32>,
    query: web::Query<GroupQuery>,
) -> Result<web::Json<HomeworkResourcesResponse>, JsonError> {
    let homework = Homework::get(&db, *homework_id)?;
    let group = Group::get(&db, homework.group_id)?;

    // Auth
    if group.secret != query.secret {
        return Err(JsonError::forbidden());
    }

    let resources = HomeworkResource::get_from_homework(&db, *homework_id)?;

    Ok(web::Json(HomeworkResourcesResponse {
        resources: resources,
    }))
}

fn generate_file_path(image_path: &str) -> (PathBuf, String) {
    let start = SystemTime::now();
    let since_the_epoch = start
        .duration_since(UNIX_EPOCH)
        .expect("Time went backwards")
        .as_millis();
    let mut p = PathBuf::new();
    let file_name = format!("{}-{}", since_the_epoch, nanoid::simple());
    p.push(format!("{}/{}", image_path, file_name));
    (p, file_name)
}

#[derive(Serialize)]
pub struct UploadHomeworkResource {
    resource: HomeworkResource,
}

pub async fn upload_homework_resource(
    db: web::Data<db::PgPool>,
    env: web::Data<Env>,
    homework_id: web::Path<i32>,
    query: web::Query<GroupQuery>,
    mut payload: Multipart,
) -> Result<web::Json<UploadHomeworkResource>, JsonError> {
    let homework = Homework::get(&db, *homework_id)?;
    let group = Group::get(&db, homework.group_id)?;

    // Auth
    if group.secret != query.secret {
        return Err(JsonError::forbidden());
    }

    let (path, file_name) = generate_file_path(&env.image_path);

    while let Some(item) = payload.next().await {
        let mut field = item?;

        // Check it's the item we want
        if let Some(content_type) = field.content_disposition() {
            if let Some(name) = content_type.get_name() {
                if name != "file" {
                    continue;
                }
            } else {
                continue;
            }
        }

        // File::create is blocking operation, use threadpool (web::block)
        let mut f = web::block(|| std::fs::File::create(path)).await?;

        // Field in turn is stream of *Bytes* object
        while let Some(chunk) = field.next().await {
            let data = match chunk {
                Ok(c) => c,
                Err(err) => {
                    println!(
                        "Got an error while trying to retrive file chunk: {:#?}",
                        err
                    );
                    return Err(JsonError::bad_request(Some(
                        "terminated while sending file",
                    )));
                }
            };
            // filesystem operations are blocking, we have to use threadpool (web::block)
            f = web::block(move || f.write_all(&data).map(|_| f)).await?;
        }

        let resource = HomeworkResource::create_entry(
            &db,
            HomeworkResourceInput {
                file_path: file_name,
                homework_id: homework.id,
            },
        )?;

        return Ok(web::Json(UploadHomeworkResource { resource: resource }));
    }

    Err(JsonError::bad_request(Some("invalid form")))
}

#[derive(Serialize)]
pub struct DeleteHomeworkResource {
    homework_id: i32,
    resources: Vec<HomeworkResource>,
}

pub async fn delete_homework_resource(
    db: web::Data<db::PgPool>,
    env: web::Data<Env>,
    resource_id: web::Path<i32>,
    query: web::Query<GroupQuery>,
) -> Result<web::Json<DeleteHomeworkResource>, JsonError> {
    let resource = HomeworkResource::get(&db, *resource_id)?;
    let homework = Homework::get(&db, resource.homework_id)?;
    let group = Group::get(&db, homework.group_id)?;

    // Auth
    if group.secret != query.secret {
        return Err(JsonError::forbidden());
    }

    let file_path = format!("{}/{}", env.image_path, resource.file_path);
    web::block(move || {
        let file_path = Path::new(&file_path);
        std::fs::remove_file(file_path)
    })
    .await?;

    HomeworkResource::delete_entry(&db, resource.id)?;

    let resources = HomeworkResource::get_from_homework(&db, homework.id)?;

    Ok(web::Json(DeleteHomeworkResource {
        homework_id: homework.id,
        resources: resources,
    }))
}
