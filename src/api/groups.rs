use actix_web::web;
use serde::{Deserialize, Serialize};

use crate::api::JsonError;
use crate::db;
use crate::models::{Group, GroupInput, GroupUpdatable, Homework};

#[derive(Deserialize)]
pub struct GroupQuery {
    pub secret: String,
}

#[derive(Serialize)]
pub struct GroupResponse {
    group: Group,
    homeworks: Vec<Homework>,
}

pub async fn get_group(
    db: web::Data<db::PgPool>,
    id: web::Path<i32>,
    query: web::Query<GroupQuery>,
) -> Result<web::Json<GroupResponse>, JsonError> {
    let group = Group::get(&db, *id)?;
    let homeworks = Homework::get_from_group(&db, *id)?;
    if group.secret == query.secret {
        Ok(web::Json(GroupResponse {
            group: group,
            homeworks: homeworks,
        }))
    } else {
        Err(JsonError::forbidden())
    }
}

pub async fn create_group(
    db: web::Data<db::PgPool>,
    body: web::Json<GroupInput>,
) -> Result<web::Json<GroupResponse>, JsonError> {
    let group = Group::create(&db, body.0)?;
    Ok(web::Json(GroupResponse {
        group: group,
        homeworks: vec![],
    }))
}

#[derive(Serialize)]
pub struct UpdateGroup {
    group: Group,
}

pub async fn update_group(
    db: web::Data<db::PgPool>,
    body: web::Json<GroupUpdatable>,
    id: web::Path<i32>,
    query: web::Query<GroupQuery>,
) -> Result<web::Json<UpdateGroup>, JsonError> {
    let group = Group::get(&db, *id)?;
    if group.secret == query.secret {
        let body = body.0;
        let group = Group::update(&db, *id, body)?;
        Ok(web::Json(UpdateGroup { group: group }))
    } else {
        Err(JsonError::forbidden())
    }
}
