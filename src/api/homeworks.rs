use actix_web::web;
use chrono::NaiveDate;
use serde::{Deserialize, Serialize};

use crate::api::groups::GroupQuery;
use crate::api::JsonError;
use crate::db;
use crate::models::{Group, Homework, HomeworkInput, HomeworkUpdateable};

#[derive(Serialize)]
pub struct GroupHomeworksResponse {
    homeworks: Vec<Homework>,
}

pub async fn get_group_homeworks(
    db: web::Data<db::PgPool>,
    id: web::Path<i32>,
    query: web::Query<GroupQuery>,
) -> Result<web::Json<GroupHomeworksResponse>, JsonError> {
    let group = Group::get(&db, *id)?;
    if group.secret == query.secret {
        let homeworks = Homework::get_from_group(&db, *id)?;
        Ok(web::Json(GroupHomeworksResponse {
            homeworks: homeworks,
        }))
    } else {
        Err(JsonError::forbidden())
    }
}

#[derive(Serialize)]
pub struct CreateHomework {
    homework: Homework,
}

#[derive(Deserialize)]
pub struct CreateHomeworkInput {
    text: String,
    deadline_date: Option<NaiveDate>,
}

pub async fn create_homework(
    db: web::Data<db::PgPool>,
    body: web::Json<CreateHomeworkInput>,
    id: web::Path<i32>,
    query: web::Query<GroupQuery>,
) -> Result<web::Json<CreateHomework>, JsonError> {
    let group = Group::get(&db, *id)?;
    if group.secret == query.secret {
        let body = body.0;
        let input = HomeworkInput {
            text: body.text,
            deadline_date: body.deadline_date,
            group_id: *id,
        };
        let homework = Homework::create(&db, input)?;
        Ok(web::Json(CreateHomework { homework: homework }))
    } else {
        Err(JsonError::forbidden())
    }
}

#[derive(Serialize)]
pub struct UpdateHomework {
    homework: Homework,
}

pub async fn update_homework(
    db: web::Data<db::PgPool>,
    body: web::Json<HomeworkUpdateable>,
    id: web::Path<i32>,
    query: web::Query<GroupQuery>,
) -> Result<web::Json<UpdateHomework>, JsonError> {
    let homework = Homework::get(&db, *id)?;
    let group = Group::get(&db, homework.group_id)?;
    if group.secret == query.secret {
        let body = body.0;
        let homework = Homework::update(&db, *id, body)?;
        Ok(web::Json(UpdateHomework { homework: homework }))
    } else {
        Err(JsonError::forbidden())
    }
}
