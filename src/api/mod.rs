use crate::db::DbError;
use actix_web::{http::StatusCode, web::HttpResponse, ResponseError};
use serde::Serialize;
use serde_json::{json, to_string_pretty};
use std::{
    convert::From,
    fmt::{Display, Formatter, Result as FmtResult},
};

pub mod groups;
pub mod homework_resources;
pub mod homeworks;

#[derive(Serialize, Debug)]
pub struct JsonError {
    error: String,
    status: u16,
}

impl JsonError {
    fn bad_request(extra_info: Option<&str>) -> Self {
        let error = match extra_info {
            Some(info) => format!("bad request, {}", info),
            None => "bad request".to_string(),
        };
        Self {
            error: error,
            status: 400,
        }
    }
    fn forbidden() -> Self {
        Self {
            error: "forbidden".to_string(),
            status: 403,
        }
    }
    fn not_found() -> Self {
        Self {
            error: "not_found".to_string(),
            status: 404,
        }
    }
    fn internal_error() -> Self {
        Self {
            error: "internal_error".to_string(),
            status: 500,
        }
    }
}

impl Display for JsonError {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        write!(f, "{}", to_string_pretty(self).unwrap())
    }
}

impl ResponseError for JsonError {
    fn error_response(&self) -> HttpResponse {
        let err_json = json!({ "error": self.error });
        HttpResponse::build(StatusCode::from_u16(self.status).unwrap()).json(err_json)
    }
}

impl From<diesel::result::Error> for JsonError {
    fn from(error: diesel::result::Error) -> Self {
        use diesel::result::Error;
        match error {
            Error::NotFound => Self::not_found(),
            _ => {
                println!("We got a Diesel error: {:#?}", error);
                Self::internal_error()
            }
        }
    }
}

impl From<diesel::r2d2::Error> for JsonError {
    fn from(_: diesel::r2d2::Error) -> Self {
        Self::internal_error()
    }
}
impl From<diesel::r2d2::PoolError> for JsonError {
    fn from(_: diesel::r2d2::PoolError) -> Self {
        Self::internal_error()
    }
}

impl From<actix_web::error::BlockingError<std::io::Error>> for JsonError {
    fn from(error: actix_web::error::BlockingError<std::io::Error>) -> Self {
        println!("We got an IO error: {:#?}", error);
        Self::internal_error()
    }
}

use actix_multipart::MultipartError;
impl From<MultipartError> for JsonError {
    fn from(error: MultipartError) -> Self {
        println!("We got a MultipartError: {:#?}", error);
        match error {
            MultipartError::NoContentType => Self::bad_request(Some("no content type")),
            MultipartError::Boundary => Self::bad_request(Some("no multipart")),
            _ => Self::internal_error(),
        }
    }
}

impl From<DbError> for JsonError {
    fn from(error: DbError) -> Self {
        match error {
            DbError::DieselError(error) => Into::into(error),
            DbError::R2d2Error(error) => Into::into(error),
            DbError::R2d2PoolError(error) => Into::into(error),
        }
    }
}
