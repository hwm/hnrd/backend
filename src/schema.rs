table! {
    groups (id) {
        id -> Int4,
        name -> Varchar,
        secret -> Varchar,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
    }
}

table! {
    homework_resources (id) {
        id -> Int4,
        file_path -> Text,
        homework_id -> Int4,
        created_at -> Timestamptz,
    }
}

table! {
    homeworks (id) {
        id -> Int4,
        text -> Text,
        archived -> Bool,
        group_id -> Int4,
        deadline_date -> Nullable<Date>,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
    }
}

joinable!(homework_resources -> homeworks (homework_id));
joinable!(homeworks -> groups (group_id));

allow_tables_to_appear_in_same_query!(groups, homework_resources, homeworks,);
