use diesel::pg::PgConnection;
use diesel::r2d2::{ConnectionManager, Pool, PoolError, PooledConnection};

pub type PgPool = Pool<ConnectionManager<PgConnection>>;
type PgPooledConnection = PooledConnection<ConnectionManager<PgConnection>>;

pub fn init_pool(database_url: &str) -> Result<PgPool, PoolError> {
    let manager = ConnectionManager::<PgConnection>::new(database_url);
    Pool::builder().build(manager)
}

pub fn get_conn(pool: &PgPool) -> Result<PgPooledConnection, DbError> {
    pool.get().map_err(Into::into)
}

#[derive(Debug)]
pub enum DbError {
    DieselError(diesel::result::Error),
    R2d2Error(diesel::r2d2::Error),
    R2d2PoolError(diesel::r2d2::PoolError),
}

impl From<diesel::result::Error> for DbError {
    fn from(error: diesel::result::Error) -> Self {
        Self::DieselError(error)
    }
}
impl From<diesel::r2d2::Error> for DbError {
    fn from(error: diesel::r2d2::Error) -> Self {
        Self::R2d2Error(error)
    }
}

impl From<diesel::r2d2::PoolError> for DbError {
    fn from(error: diesel::r2d2::PoolError) -> Self {
        Self::R2d2PoolError(error)
    }
}
