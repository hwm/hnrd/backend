use std::env;

pub struct Env {
    pub image_path: String,
}

impl Env {
    pub fn init() -> Self {
        Self {
            image_path: env::var("IMAGE_PATH").expect("IMAGE_PATH tiene que estar seteado"),
        }
    }
}
