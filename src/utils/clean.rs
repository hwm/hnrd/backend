use crate::{
    db::{get_conn, DbError, PgPool},
    env::Env,
    models::{homework::Homework, homework_resource::HomeworkResource},
};
use diesel::{BoolExpressionMethods, Connection, ExpressionMethods, QueryDsl, RunQueryDsl};
use std::{fs};

// XXX: there's a thousand ways to optimize this

pub fn clean(pool: &PgPool, env: &Env) -> Result<(), DbError> {
    dotenv::dotenv().ok();

    println!("Cleaning stuff...");

    {
        use crate::schema::{homework_resources, homeworks};
        use diesel::dsl::*;

        let conn = get_conn(pool)?;
        conn.transaction::<_, diesel::result::Error, _>(|| {
            let criteria = homeworks::dsl::archived
                // TODO: change to "now - 7.days()" when this issue is resolved:
                // https://github.com/diesel-rs/diesel/issues/1514
                .and(homeworks::dsl::updated_at.lt(sql("NOW() - INTERVAL '7 days'")));
            let discardable_homeworks: Vec<Homework> = homeworks::dsl::homeworks
                .filter(criteria.clone())
                .get_results::<Homework>(&conn)?;

            let ids: Vec<i32> = discardable_homeworks
                .into_iter()
                .map(|homework| homework.id)
                .collect();

            diesel::delete(
                homework_resources::dsl::homework_resources.filter(homework_resources::dsl::homework_id.eq(any(ids))),
            )
            .execute(&conn)?;
            diesel::delete(homeworks::dsl::homeworks.filter(criteria.clone())).execute(&conn)?;

            Ok(())
        })?;
    }

    let resources = HomeworkResource::all(pool)?
        .into_iter()
        .map(|res| res.file_path)
        .collect::<Vec<String>>();

    for entry in fs::read_dir(&env.image_path).unwrap() {
        let entry = entry.unwrap();
        let path = entry.path();
        if path.is_file() {
            let file_name = path.file_name().unwrap().to_str().unwrap();
            let is_in_db = resources.iter().any(|res| res.eq(file_name));
            if is_in_db {
                // println!("{:?} esta en la base de datos, ignorando...", file_name);
            } else {
                // println!("{:?} no esta en la base de datos, eliminando...", file_name);
                fs::remove_file(path).unwrap();
            }
        } else {
            // println!("{:?} no es un archivo, ignorando...", path);
        }
    }
    Ok(())
}
