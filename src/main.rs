#[macro_use]
extern crate diesel;
#[macro_use]
extern crate diesel_migrations;

use actix_cors::Cors;
use actix_files as fs;
use actix_web::{http, middleware::Logger, web, App, HttpResponse, HttpServer, Responder};
use env::Env;
use std::{io, thread, time::Duration};

mod api;
mod db;
mod env;
mod models;
mod schema;
mod utils;

async fn index() -> impl Responder {
    r#"homework-manager API

Código: https://git.homework-manager.ml/homework-manager/?

  == RUTAS =="#
}

async fn not_found() -> impl Responder {
    HttpResponse::build(http::StatusCode::NOT_FOUND)
        .header(http::header::CONTENT_TYPE, "text/html")
        .body("Not found. Back to <a href=/>root</a>.")
}

embed_migrations!();

#[actix_rt::main]
async fn main() -> io::Result<()> {
    dotenv::dotenv().ok();

    // logger
    std::env::set_var("RUST_LOG", "actix_web=info");
    env_logger::init();

    // base de datos
    let database_url = std::env::var("DATABASE_URL").expect("DATABASE_URL tiene que estar seteado");
    let pool = db::init_pool(&database_url).expect("falló al crear la pool");
    embedded_migrations::run_with_output(
        &db::get_conn(&pool).expect("no se pudo correr las migraciones"),
        &mut io::stdout(),
    )
    .map_err(|err| io::Error::new(io::ErrorKind::Other, err))?;

    // cron
    let thread_pool = pool.clone();
    thread::spawn(move || loop {
        utils::clean(&thread_pool.clone(), &Env::init()).expect("Failed to run cron");
        thread::sleep(Duration::from_millis(1000 * 60 * 60));
    });

    let server_address = std::env::var("LISTEN_ON").expect("LISTEN_ON tiene que estar seteado");
    println!("Starting server @ http://{}", server_address);

    let app = move || {
        println!("Creating app");

        let env = Env::init();
        let image_path = env.image_path.clone();

        App::new()
            .data(pool.clone())
            .data(env)
            .wrap(Logger::default())
            .wrap(Cors::new().max_age(3600).finish())
            .service(web::resource("/").to(index))
            .service(web::resource("/groups").route(web::post().to(api::groups::create_group)))
            .service(
                web::resource("/groups/{id}")
                    .route(web::get().to(api::groups::get_group))
                    .route(web::post().to(api::groups::update_group)),
            )
            .service(
                web::resource("/groups/{id}/homeworks")
                    .route(web::get().to(api::homeworks::get_group_homeworks))
                    .route(web::post().to(api::homeworks::create_homework)),
            )
            .service(
                web::resource("/homeworks/{id}")
                    //.route(web::get().to(api::homeworks::XXX))
                    .route(web::post().to(api::homeworks::update_homework)),
            )
            .service(
                web::resource("/homeworks/{id}/resources")
                    .route(web::get().to(api::homework_resources::get_homework_resources))
                    .route(web::post().to(api::homework_resources::upload_homework_resource)),
            )
            .service(
                web::resource("/homework_resources/{id}")
                    .route(web::delete().to(api::homework_resources::delete_homework_resource)),
            )
            .service(fs::Files::new("/image_uploads", image_path))
            .default_service(web::route().to(not_found))
    };

    HttpServer::new(app).bind(server_address)?.run().await
}
