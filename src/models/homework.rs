use crate::db::{get_conn, DbError, PgPool};
use crate::schema::{homeworks, homeworks::dsl};
use chrono::{NaiveDate, NaiveDateTime};
use diesel::prelude::*;
use serde::{Deserialize, Serialize};
use std::ops::Deref;

#[derive(Queryable, Debug, Serialize)]
pub struct Homework {
    pub id: i32,
    pub text: String,
    pub archived: bool,
    pub group_id: i32,
    pub deadline_date: Option<NaiveDate>,
    pub created_at: NaiveDateTime,
    pub updated_at: NaiveDateTime,
}

#[derive(Deserialize)]
pub struct HomeworkInput {
    pub text: String,
    pub group_id: i32,
    pub deadline_date: Option<NaiveDate>,
}

#[derive(Insertable)]
#[table_name = "homeworks"]
pub struct HomeworkInsertable {
    pub text: String,
    pub group_id: i32,
    pub deadline_date: Option<NaiveDate>,
    pub archived: bool,
}

#[derive(AsChangeset, Deserialize)]
#[table_name = "homeworks"]
pub struct HomeworkUpdateable {
    pub text: Option<String>,
    #[serde(default)]
    #[serde(deserialize_with = "deserialize_optional_field")]
    pub deadline_date: Option<Option<NaiveDate>>,
    pub archived: Option<bool>,
}

// hack: https://stackoverflow.com/questions/44331037/how-can-i-distinguish-between-a-deserialized-field-that-is-missing-and-one-that
fn deserialize_optional_field<'de, T, D>(deserializer: D) -> Result<Option<Option<T>>, D::Error>
where
    D: serde::Deserializer<'de>,
    T: Deserialize<'de>,
{
    Ok(Some(Option::deserialize(deserializer)?))
}

impl HomeworkInsertable {
    fn new(input: HomeworkInput) -> Self {
        Self {
            text: input.text,
            group_id: input.group_id,
            deadline_date: input.deadline_date,
            archived: false,
        }
    }
}

impl Homework {
    fn all(pool: &PgPool) -> Result<Vec<Homework>, DbError> {
        dsl::homeworks
            .load::<Homework>(get_conn(pool)?.deref())
            .map_err(Into::into)
    }

    pub fn get(pool: &PgPool, id: i32) -> Result<Homework, DbError> {
        dsl::homeworks
            .filter(dsl::id.eq(id))
            .limit(1)
            .get_result::<Homework>(get_conn(pool)?.deref())
            .map_err(Into::into)
    }

    pub fn get_from_group(pool: &PgPool, group_id: i32) -> Result<Vec<Homework>, DbError> {
        dsl::homeworks
            .filter(dsl::group_id.eq(group_id))
            .get_results::<Homework>(get_conn(pool)?.deref())
            .map_err(Into::into)
    }

    pub fn create(pool: &PgPool, input: HomeworkInput) -> Result<Homework, DbError> {
        let input = HomeworkInsertable::new(input);
        diesel::insert_into(homeworks::table)
            .values(&input)
            .get_result(get_conn(pool)?.deref())
            .map_err(Into::into)
    }

    pub fn update(pool: &PgPool, id: i32, update: HomeworkUpdateable) -> Result<Homework, DbError> {
        diesel::update(dsl::homeworks.filter(dsl::id.eq(id)))
            .set(&update)
            .get_result(get_conn(pool)?.deref())
            .map_err(Into::into)
    }
}
