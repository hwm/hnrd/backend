use crate::db::{get_conn, DbError, PgPool};
use crate::schema::{homework_resources, homework_resources::dsl};
use chrono::NaiveDateTime;
use diesel::prelude::*;
use serde::Serialize;
use std::ops::Deref;

#[derive(Queryable, Debug, Serialize)]
pub struct HomeworkResource {
    pub id: i32,
    pub file_path: String,
    pub homework_id: i32,
    pub created_at: NaiveDateTime,
}

#[derive(Insertable)]
#[table_name = "homework_resources"]
pub struct HomeworkResourceInput {
    pub file_path: String,
    pub homework_id: i32,
}

impl HomeworkResource {
    pub fn all(pool: &PgPool) -> Result<Vec<HomeworkResource>, DbError> {
        dsl::homework_resources
            .load::<HomeworkResource>(get_conn(pool)?.deref())
            .map_err(Into::into)
    }

    pub fn get(pool: &PgPool, id: i32) -> Result<HomeworkResource, DbError> {
        dsl::homework_resources
            .filter(dsl::id.eq(id))
            .limit(1)
            .get_result::<HomeworkResource>(get_conn(pool)?.deref())
            .map_err(Into::into)
    }

    pub fn get_from_homework(
        pool: &PgPool,
        homework_id: i32,
    ) -> Result<Vec<HomeworkResource>, DbError> {
        dsl::homework_resources
            .filter(dsl::homework_id.eq(homework_id))
            .get_results::<HomeworkResource>(get_conn(pool)?.deref())
            .map_err(Into::into)
    }

    pub fn create_entry(
        pool: &PgPool,
        input: HomeworkResourceInput,
    ) -> Result<HomeworkResource, DbError> {
        diesel::insert_into(homework_resources::table)
            .values(&input)
            .get_result(get_conn(pool)?.deref())
            .map_err(Into::into)
    }

    pub fn delete_entry(pool: &PgPool, resource_id: i32) -> Result<(), DbError> {
        diesel::delete(dsl::homework_resources.filter(dsl::id.eq(resource_id)))
            .execute(get_conn(pool)?.deref())
            .map(|_| ())
            .map_err(Into::into)
    }
}
