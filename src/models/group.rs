use chrono::NaiveDateTime;
use diesel::prelude::*;
use serde::{Deserialize, Serialize};
use std::ops::Deref;

use crate::db::{get_conn, DbError, PgPool};
use crate::schema::{groups, groups::dsl};

#[derive(Queryable, Debug, Serialize)]
pub struct Group {
    pub id: i32,
    pub name: String,
    pub secret: String,
    pub created_at: NaiveDateTime,
    pub updated_at: NaiveDateTime,
}

#[derive(Deserialize)]
pub struct GroupInput {
    pub name: String,
}

#[derive(Insertable)]
#[table_name = "groups"]
struct GroupInsertable {
    name: String,
    secret: String,
}

#[derive(AsChangeset, Deserialize)]
#[table_name = "groups"]
pub struct GroupUpdatable {
    pub name: String,
}

impl GroupInsertable {
    fn new(input: GroupInput) -> Self {
        Self {
            name: input.name,
            secret: nanoid::simple(),
        }
    }
}

impl Group {
    fn all(pool: &PgPool) -> Result<Vec<Group>, DbError> {
        dsl::groups
            .load::<Group>(get_conn(pool)?.deref())
            .map_err(Into::into)
    }

    pub fn get(pool: &PgPool, id: i32) -> Result<Group, DbError> {
        dsl::groups
            .filter(dsl::id.eq(id))
            .limit(1)
            .get_result::<Group>(get_conn(pool)?.deref())
            .map_err(Into::into)
    }

    pub fn create(pool: &PgPool, input: GroupInput) -> Result<Group, DbError> {
        let input = GroupInsertable::new(input);
        diesel::insert_into(groups::table)
            .values(&input)
            .get_result(get_conn(pool)?.deref())
            .map_err(Into::into)
    }

    pub fn update(pool: &PgPool, id: i32, update: GroupUpdatable) -> Result<Group, DbError> {
        diesel::update(dsl::groups.filter(dsl::id.eq(id)))
            .set(&update)
            .get_result(get_conn(pool)?.deref())
            .map_err(Into::into)
    }
}
