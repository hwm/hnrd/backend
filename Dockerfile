FROM docker.io/rust:1.70 as builder
RUN apt-get update && apt-get install -y libpq-dev && rm -rf /var/lib/apt/lists/*
WORKDIR /usr/src/hnrd
COPY . .
RUN cargo install --path .

FROM docker.io/debian:bullseye-slim
RUN apt-get update && apt-get install -y libpq5 && rm -rf /var/lib/apt/lists/*
COPY --from=builder /usr/local/cargo/bin/hnrd-backend /usr/local/bin/

ENV LISTEN_ON=0.0.0.0:8000
EXPOSE 8000
ENV IMAGE_PATH=/image_uploads
# ENV DATABASE_URL

CMD ["hnrd-backend"]
