# hnrd/backend

## levantar

necesita Rust (minimo `1.39`) y PostgreSQL (minimo v9.6).

```bash
$ sudo -u postgres psql
```
```sql
=# CREATE DATABASE hnrd_dev;
=# \password
# elegir contraseña para la base de datos
=# \q
```
```bash
# editar .env, crear image_uploads
$ cargo run --bin hnrd-backend
```
