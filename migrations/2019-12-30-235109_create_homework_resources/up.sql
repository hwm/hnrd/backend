CREATE TABLE homework_resources(
  id SERIAL PRIMARY KEY,
  file_path TEXT NOT NULL,
  homework_id INTEGER NOT NULL REFERENCES homeworks(id),
  created_at TIMESTAMPTZ NOT NULL DEFAULT NOW()
); 
