CREATE TABLE homeworks(
  id SERIAL PRIMARY KEY,
  text TEXT NOT NULL,
  archived BOOLEAN NOT NULL,
  group_id INTEGER NOT NULL REFERENCES groups(id),
  deadline_date DATE NULL,
  created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
  updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW()
);

CREATE TRIGGER set_homeworks_timestamp
BEFORE UPDATE ON homeworks
FOR EACH ROW
EXECUTE PROCEDURE trigger_set_timestamp();
